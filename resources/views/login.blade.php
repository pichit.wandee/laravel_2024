<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <h1>Form Validation Example</h1>
    <form action="{{route('login.submit')}}" method="post">
        @csrf
        <div class="mb-3">
            <label for="email">Email :</label>
            <input type="email" name="email" id="email" class="form-control">
            @error('email')
                <span class="text-danger">{{$message}}</span> 
            @enderror
        </div>
        <div class="mb-3">
            <label for="password">Password :</label>
            <input type="password" name="password" id="password" class="form-control">
            @error('password')
                <span class="text-danger">{{$message}}</span> 
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</body>
</html>