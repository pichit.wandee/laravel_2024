<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User page</title>
  </head>
  <body>
    <h1>Hello, User!</h1>
    <p>Name : {{ $users['name'] }} </p>
    <p>Email : {{ $users['email'] }} </p>
    <p>Tel : {{ $users['tel'] }} </p>
  </body>
</html>