<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Models\Teacher;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/about', function () {
//     // return view('welcome');
//     return view('about');
// });

// Route::get('about','about');

Route::get('about', function(){
    return view('about');
});

// Route::get('/user',[UserController::class,'index']);

Route::controller(UserController::class)->group(function(){
         Route::get('/user','user');
         Route::get('/home','home');
         Route::get('/noaccess','noaccess');
         Route::get('/user-2','index');
         Route::get('/login','login');
         Route::post('login','loginSubmit')->name('login.submit');
    }
);

Route::controller(PostController::class)->group(function(){
    Route::get('/post','getAllPost');
    }
);

Route::controller(CommentController::class)->group(function(){
    Route::get('/comment','getComment');
    }
);

Route::controller(StudentController::class)->group(function(){
    Route::get('/student','Student');
    }
);

Route::controller(TeacherController::class)->group(function(){
    Route::get('/teacher','TeacherData');
    }
);

