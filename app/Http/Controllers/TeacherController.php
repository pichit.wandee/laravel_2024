<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeacherController extends Controller
{
    public function TeacherData()
    {
        // return DB::table('teachers')->get();

        //Insert Data       
        // return DB::table('teachers')
        // ->insert([
        //     'name' => 'Poala',
        //     'department' => 'Math',
        //     'email' => 'paola@test.com'
        // ]);

        //Update Data
        // return DB::table('teachers')
        // ->where('id',5)
        // ->update([
        //     'name' => 'Smith',
        //     'department' => 'Statistics',
        //     'email' => 'smith@test.com'
        // ]);

        //Delete Data
        return DB::table('teachers')
        ->where('id',5)->delete();
    }
}
