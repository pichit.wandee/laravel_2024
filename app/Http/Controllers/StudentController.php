<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    public function Student()
    {
        // $student = Student::all();
        // $student = Student::find(3);
        // $student = Student::where('advisor_id',2)->get();

        // $student = Student::select('name')->get();
        // $student = Student::pluck('name');
        //  $student = Student::count('name');
        //   $student = Student::sum('age');
        //   $student = Student::avg('age');
        //   $student = Student::max('age');

        /*$student = Student::select('name','department')
        ->where('advisor_id',1)
        ->orderby('name','desc')
        ->get();*/

        $student = Student::select(Student::raw('count(*) as stu_count, advisor_id'))
        ->groupby('advisor_id')->get();

        return $student;
    }
}
