<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        // return $request->method();
        // return $request->path();
        return $request->fullUrl();
        
        // echo "Hello from UserController";
    }
    
    // user
    // public function User()
    // {
    //     $users = array(
    //         "name" => "Peter W",
    //         "email" => "Peter.W@test.com",
    //         "tel" => "064221451",
    //     );

        // return view('user',compact('users'));
    // }
    public function login()
    {
        return view('login');
    }
    public function loginSubmit(Request $request)
    {
        $validateData = $request->validate([
            'email' => 'required:email',
            'password' => 'required|min:6|max:12'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        return 'Email :'.$email. 'Password :'.$password;
    }
    
    //////////MiddleWare
    public function home()
    {
        return view('home');
    }
    public function user()
    {
        return view('user');
    }
    public function noaccess()
    {
        return view('noaccess');
    }
    

}
